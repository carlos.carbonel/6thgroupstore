import { flatten } from '@angular/compiler';
import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  appForm = this.fb.group({
    // user: this.fb.group({
    //   loginUser: ['dmin@6th-shop.com', [Validators.required, Validators.email]],
    //   passwordUser: ['Admin123', [Validators.required, Validators.minLength(6)]]
    // }),

    loginUser: ['', [Validators.required, Validators.email]],
    passwordUser: ['', [Validators.required, Validators.minLength(6)]]
  });

  title = 'frontend';

  //Dato para iiciar sesión


  //Para mostrar Modal o desplazar opciones de usuario.
  logout: boolean=true;
  login: boolean=false;
  onSesion(){
    // if(this.logout){
    //   this.login=
    // }
    this.logout=!this.login;
    this.login=!this.logout;
    //Si no ha iniciado sesión, se muestra form de login
    if(this.logout){
      this.boolSesion=true;
      this.boolRecovery=false;
      this.boolRegister=false;
    }
  }

  //Para mostrar formulario de Sesión
  boolSesion: boolean=false;
  togleSesion(){
    this.boolSesion=true;
    if(this.boolSesion){
      this.boolRecovery=false;
      this.boolRegister=false;
    }
  }
  //Para mostrar formulario de recuperación
  boolRecovery: boolean=false;
  togleRecovery(){
    this.boolRecovery=true;
    if(this.boolRecovery){
      this.boolSesion=false;
      this.boolRegister=false;
    }
  }
  //Para mostrar formulario de Registro
  boolRegister: boolean=false;
  togleRegister(){
    this.boolRegister=true;
    if(this.boolRegister){
      this.boolRecovery=false;
      this.boolSesion=false;
    }
  }



  loadLogin(){

    let x = this.appForm.get('loginUser')?.value.toLowerCase();
    let y = this.appForm.get('passwordUser')?.value;
    if(x=="admin@6th-store.com" && y=="Admin123"){
      // Cargar los datos del cliente, y esconder iniciar sesion
      this.login=true;
      this.logout=false;
     // Response.redirect("../");
     // document.getElementById('labelUser').innerHTML = val;
    }else{
      alert("Usuario o contraseña incorrecto");
    }
//   "admin@6th-shop.com", "passwordUser": "Admin123" }

  }

  // dcont: boolean = false;
  // toggleCont() {
  //   this.dcont = !this.dcont;
  //   if (this.dcont) {
  //     this.dprof = !this.dcont;
  //     this.dpay = !this.dcont;
  //   }
  //   else {
  //     this.dcont = true;
  //   }
  // }


  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.onSesion();
  }

}




