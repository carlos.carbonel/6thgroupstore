import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ContactComponent } from "./views/contact/contact.component";
import { CartComponent } from "./views/cart/cart.component";
import { HomeComponent } from "./views/home/home.component";
import { OffersComponent } from "./views/offers/offers.component";
import { ProfileComponent } from "./account/profile/profile.component";
import { HistorialComponent } from "./views/historial/historial.component";
import { AboutusComponent } from "./views/aboutus/aboutus.component";
import { ProductComponent } from "./views/product/product.component";
import { CategoryComponent } from "./views/category/category.component";

const routes: Routes = [
  { path: 'contact', component: ContactComponent },
  { path: 'cart', component: CartComponent },
  { path: 'home', component: HomeComponent },
  { path: 'offers', component: OffersComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'historial', component: HistorialComponent },
  { path: 'about', component: AboutusComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'product', component: ProductComponent },
  { path: 'category', component: CategoryComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
