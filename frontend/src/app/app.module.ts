import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { ContactComponent } from './views/contact/contact.component';
import { OffersComponent } from './views/offers/offers.component';
import { CartComponent } from './views/cart/cart.component';
import { ProfileComponent } from './account/profile/profile.component';

import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from './account/login/login.component';
import { HistorialComponent } from './views/historial/historial.component';
import { AboutusComponent } from './views/aboutus/aboutus.component';
import { ProductComponent } from './views/product/product.component';
import { CategoryComponent } from './views/category/category.component';
// Agregar ReactiveForms para el manejo de formularios
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ContactComponent,
        OffersComponent,
        CartComponent,
        ProfileComponent,
        LoginComponent,
        HistorialComponent,
        AboutusComponent,
        ProductComponent,
        CategoryComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
