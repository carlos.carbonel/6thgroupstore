﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;   

namespace UPC.APIBusiness.API.Controllers
{   /// <summary>
/// 
/// </summary>
    [Produces("application/json")]
    [Route("api/project")]
    public class ProjectController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IProjectRepository _ProjectRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProjectRepository"></param>

        public ProjectController(IProjectRepository ProjectRepository)
        {
            _ProjectRepository = ProjectRepository;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Produces ("application/json")]
        [AllowAnonymous]
        [HttpGet]
        [Route ("GetProjects")]
        public ActionResult GetProject()
        {
            var ret = _ProjectRepository.GetProjects();
            if (ret == null)
                return StatusCode(401);

            return Json(ret);

        }
        
    }
}
