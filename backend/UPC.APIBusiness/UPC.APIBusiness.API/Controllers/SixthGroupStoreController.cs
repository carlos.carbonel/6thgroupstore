﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{/// <summary>
/// 
/// </summary>
    [Produces("application/json")]
    [Route("api/SixthGroupStore")]
    public class SixthGroupStoreController : Controller
    {/// <summary>
     /// 
     /// </summary>
        protected readonly ISixthGroupStoreRepository _SixthGroupRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sixthGroupStoreRepository"></param>
        public SixthGroupStoreController(ISixthGroupStoreRepository sixthGroupStoreRepository)
        {
            _SixthGroupRepository = sixthGroupStoreRepository;
        }
        [Produces ("application/json")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetProductos")]
        public ActionResult GetProducto()
        {
            var ret = _SixthGroupRepository.GetProductos();
            if (ret == null)
                return StatusCode(401);
            return Json(ret);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idProducto"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetProductosById")]
        public ActionResult GetProductoById(int idProducto)
        {
            var ret = _SixthGroupRepository.GetProductoById(idProducto);
            if (ret == null)
                return StatusCode(401);
            return Json(ret);
        }
        
    }
}
