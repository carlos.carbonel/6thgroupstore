﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface ISixthGroupStoreRepository
    {
        List<EntityProducto> GetProductos();
        EntityProducto GetProductoById(int id);
    }
}


