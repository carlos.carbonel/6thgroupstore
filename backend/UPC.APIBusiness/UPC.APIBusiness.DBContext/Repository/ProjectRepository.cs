﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace DBContext
{
    public class ProjectRepository : BaseRepository, IProjectRepository
    {
        public List<EntityProject> GetProjects()
        {
            var returnEntity = new List<EntityProject>();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Listar_Proyectos ";

                    returnEntity = db.Query<EntityProject>(sql, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnEntity;
        }
    }
}
