﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace DBContext
{
    public class SixthGroupStoreRepository : BaseRepository, ISixthGroupStoreRepository
    {
               public EntityProducto GetProductoById(int id)
        {
            var returnEntity = new EntityProducto();
            try
            {
                using (var db= GetSqlConnection())
                {
                    const string sql = @"usp_Obtener_Productos";
                    var p = new DynamicParameters();
                    p.Add(name: "@IDPROYECTO", value: id, DbType.Int32, direction: ParameterDirection.Input);

                    returnEntity = db.Query<EntityProducto>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnEntity;
        }

        public List<EntityProducto> GetProductos()
        {
            var returnEntity = new List<EntityProducto>();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Obtener_Productos";

                    returnEntity = db.Query<EntityProducto>(sql, commandType: CommandType.StoredProcedure).ToList();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnEntity;
        }
    }
}   

